def free_params(*, diag_re=[1,1], ortho_re=True, k_fe=2):

    n_re = len(diag_re)
    if ortho_re:
        cov_re = np.diag(diag_re).astype(int)
    else:
        cov_re = np.ones((n_re,n_re), dtype=int) + np.diag(-1 + np.array(diag_re, dtype=int))

    return sm.regression.mixed_linear_model.MixedLMParams.from_components(
                fe_params=np.ones(k_fe, dtype=int),
                cov_re=cov_re
                )


def lmm_flex(*, method, data,
             y_name="PUTAMEN",
             x_name="TIME_normalized",
             groups_name="ID",
             start_std=[1.,1.], start_corr=0.,
             fix_std=[False, False], fix_corr=False,
             return_res=False, plot_re=True):

    if method in {'powell','nm'}:
        if True in fix_std or fix_corr:
            raise ValueError('Powell / NM cannot handle free parameter...')

    formula = "% ~ %s +1" % (y_name, x_name)
    re_formula = "~%s + 1" % x_name

    # ---- Estimate only the fixed parameters and the intercept variance
    #      (independence btw random intercept and random slope enforced,
    #       random slope variance fixed in `start_params`)
    free = free_params(diag_re=[not fix_std[0], not fix_std[1]], ortho_re=fix_corr, k_fe=2)

    # ---- Set starting values for fixed parameters and covariance matrix of random effects

    # all start params all now unscaled <!>
    start_cov = start_corr * start_std[0] * start_std[1]
    start_params = sm.regression.mixed_linear_model.MixedLMParams.from_components(
                        #fe_params=np.array([0., 1e-2]),
                        cov_re=np.array([[start_std[0]**2, start_cov],
                                         [start_cov, start_std[1]**2]])
    ) # <!> unscaled

    # new model (TIME_norm)
    lmm_model = smf.mixedlm(formula=formula,
                            data=data.xs('train', level='SPLIT').reset_index(),
                            groups=groups_name, re_formula=re_formula)

    lmm = lmm_model.fit(method=method, free=free, start_params=start_params)
    #print(lmm.summary())

    ## --- Reconstruction on train/test

    random_effects_df = pd.DataFrame(lmm.random_effects).T.rename_axis(groups_name)
    random_effects_df.columns = ['Random intercept', 'Random slope']
    random_effects_df['INTERCEPT'] = random_effects_df['Random intercept'] + lmm.fe_params.loc['Intercept']
    random_effects_df['SLOPE'] = random_effects_df['Random slope'] + lmm.fe_params.loc[x_name]
    #random_effects_df.head()

    y_model = random_effects_df['INTERCEPT'] + all_patients_ages * random_effects_df['SLOPE']
    y_model_err = y_model - putamen_df[y_name]

    # check rmse train
    rmse = (y_model_err ** 2).groupby('SPLIT').mean() ** .5

    rmse_train = (lmm.resid ** 2).mean() ** .5
    assert abs(rmse['train'] - rmse_train) < 1e-6  # <=> residual variance on train

    # ---- Plot random effects

    true_std_ri = lmm.cov_re.loc[groups_name, groups_name] ** .5
    true_std_rs = lmm.cov_re.loc[x_name, x_name] ** .5

    d = {
        # infered cov_re "scaled" = lmm.cov_re_unscaled * lmm.scale (<!> given start_params.cov_re is unscaled)
        'std_ri': true_std_ri,
        'std_rs': true_std_rs,

        'std_ri_unsc': true_std_ri / lmm.scale ** .5,
        'std_rs_unsc': true_std_rs / lmm.scale ** .5,

        'corr': lmm.cov_re.loc[groups_name,x_name]/(true_std_rs*true_std_ri), # correlation term

        'sigma': lmm.scale**.5,
        'll': lmm.llf,
        # rmse
        **rmse.to_dict(),
    }

    #reg_re = smf.ols(formula='Q("Random slope") ~ 1 + Q("Random intercept")', data=random_effects_df).fit()
    #d['exp_corr'] = reg_re.rsquared**.5 * np.sign(reg_re.params['Q("Random intercept")'])
    d['exp_corr'] = random_effects_df[['Random intercept','Random slope']].corr().iloc[0,1]

    if plot_re:
        with sns.axes_style('whitegrid'):
            sns.jointplot(x='Random intercept', y='Random slope', data=random_effects_df, kind='reg')

    if return_res:
        d['res'] = lmm

    return d
